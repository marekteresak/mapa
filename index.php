<!DOCTYPE html>
<html lang="cz">
    <head>
        <title>Mapa</title>
        <meta name="viewport">
        <link rel="stylesheet" href="./styles/css/styles.css" media="screen">
        <script src="./scripts/jquery.js"></script>
        <script src="./scripts/main.js"></script>
        <script src="http://maps.googleapis.com/maps/api/js?key=&senzor=false&library=places"></script>
        <?php
        include_once(__DIR__ . '/includes/db/connect.php');
        include_once('links.php');
        ?>
    </head>
    <body>
        <section id="wrap">
            <div id="wrapInner">
                <section class="header">
                    <div class="logo">
                        <img src="pictures/logo.png" />
                        <ul class="ui-items">
                            <li><a href="#">Nastavení</a></li>
                            <li><strong>|</strong></li>
                            <li><a href="#">Odhlásit se</a></li>
                        </ul>
                    </div>
                    <div class="headerInner">
                        <ul class="navigator">
                            <li><a href="#">Úvodní<br>informace</a></li>
                            <li><a href="#">Statistiky<br>a provize</a></li>
                            <li><a href="#">Vložit<br>podmínky</a></li>
                            <li><a href="#">Přehled<br>podniků</a></li>
                            <li><a href="#">Výplaty<br>(k výplate: 19 296,- kč)</a></li>
                        </ul>
                    </div>
                </section>
                <section id="body" class="page active body">
                    <div class="innerBox">
                        <div id="filter">
                            <form action="#" method="post">
                                <fieldset id="scheduler">
                                    <span>
                                        <select class="field" title="Všechna města">
                                            <?php
                                            for ($i = 0; $i < mysqli_num_fields($result); $i++) {
                                                while ($field = mysqli_fetch_row($result)) {
                                                    foreach ($field as $item) {
                                                        echo('<option value="stores" name="stores" id="cities">');
                                                        echo sprintf($item);
                                                        echo('</option>');?>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                            <?php
                                            mysqli_free_result($result);
                                            ?>
                                        </select>
                                    </span>
                                    <span>
                                        <select class="field" title="Všechny regiony">
                                            <?php
                                            for ($i = 0; $i < mysqli_num_fields($result_2); $i++) {
                                                while ($field = mysqli_fetch_row($result_2)) {
                                                    foreach ($field as $item) {
                                                        echo('<option value="regions" name="regions" id="regions">');
                                                        echo sprintf($item);
                                                        echo('</option>');?>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                            <?php
                                            mysqli_free_result($result_2);
                                            ?>
                                        </select>
                                    </span>
                                    <span>
                                        <select class="field" title="Všechny podniky">
                                            <?php
                                            for ($i = 0; $i < mysqli_num_fields($result_3); $i++) {
                                                while ($field = mysqli_fetch_row($result_3)) {
                                                    foreach ($field as $item) {
                                                        echo('<option value="stores" name="stores" id="stores">');
                                                        echo sprintf($item);
                                                        echo('</option>');?>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                            <?php
                                            mysqli_free_result($result_3);
                                            ?>
                                        </select>
                                    </span>
                                    <input type="submit" value="search" class="button" />
                                </fieldset>
                            </form>
                        </div>
                    </div>
                    <div id="map-canvas">
                        <?php include_once('links.php');?>
                    </div>
                </section>
            </div>
        </section>
    </body>
</html>