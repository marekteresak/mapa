$(function() {
    var mapLoc;
    var prague;
    var myOptions;
    var instance = 0;
    var data = false;
    var highlightId = 0;

    var markerImage;

    function Init()
    {

        if ( navigator.geolocation )
            {
                navigator.geolocation.getCurrentPosition(UserLocation);
            }
        else
            {
                prague = new google.maps.LatLng(50.0605747, 14.4626192);
            }
    }

    function initialize(item) {
        highlightId = item;
        if (!data) {
            data = true;
        }

        setTimeout(function () {
            data = true;
            if (instance == 0) return;

            $('#cities-' + highlightId).addClass('observed');
            $('#regions-' + highlightId).addClass('observed');
            $('#stores-' + highlightId).addClass('observed');

        }, 200);
    }

    function setMapListener()
    {
        google.maps.event.addListener(map, 'idle', function ()
        {
            var map = mapLoc.getBounds();

            $.post(
                ''
            )
        })
    }

        myOptions = {
            center: prague,
            zoom: 12,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        mapLoc = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
        //markerImage = new google.maps.MarkerImage('/pictures/icon-2.png', new google.maps.Size(50, 70));

    function UserLocation( position )
    {
        ShowLocation( position.coords.latitude, position.coords.longitude, "This is your Location" );
    }

    function ShowLocation( lat, lng, title )
    {
        var contentString;
        var infowindow;
        var marker;
        var latlng = new google.maps.LatLng( lat, lng );

        var mapOptions = { zoom: 18, center: latlng  };

        mapLoc = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

        marker = new google.maps.Marker( {
            position: latlng,
            map: mapLoc,
            title: title,
            type: 'food',
            icon: markerImage
        });

        contentString = "<b>" + title + "</b>";
        infowindow = new google.maps.InfoWindow( { content: contentString } );

        google.maps.event.addListener( marker, 'click', function() { infowindow.open( mapLoc, marker ); });
    }

    google.maps.event.addDomListener( window, 'load', Init );

});